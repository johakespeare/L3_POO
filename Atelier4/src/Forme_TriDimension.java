public abstract class Forme_TriDimension extends Forme {
    //* CONSTRUCTEURS */
    public Forme_TriDimension(String nom) {
         //*@param nom , le nom de la forme
        */
        super(nom);
    }

    //*METHODES */
    public abstract double volume();

}
